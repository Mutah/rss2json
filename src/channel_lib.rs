use std::fs::File;
use std::io::{BufReader, Cursor};

use log::{error, info};
pub use rss::Channel;

pub fn build_channel(input: &String) -> Channel {
    let channel: Channel;

    if input.starts_with("http://") || input.starts_with("https://") {
        // process download
        match reqwest::blocking::get(input).unwrap().bytes() {
            Ok(content) => {
                info!("Extracted {} bytes", content.len());
                let cursor = Cursor::new(content);
                channel = Channel::read_from(cursor).unwrap();
            }
            Err(e) => {
                error!("{}", e);
                std::process::exit(1);
            }
        }
    } else {
        // read locale file
        let file = File::open(input).unwrap();
        channel = Channel::read_from(BufReader::new(file)).unwrap();
    }

    return channel;
}
