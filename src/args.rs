use clap::Parser;

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
pub struct Args {
    #[clap(short, long, required = true)]
    pub input: String,

    #[clap(short, long, default_value_t=String::new())]
    pub output: String,

    #[clap(short, long, default_value_t = false)]
    pub pretty: bool,

    #[clap(long, default_value_t = 4)]
    pub indentation: u16,
}

pub fn parse_args()-> Args {
    return Args::parse();
}