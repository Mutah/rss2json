/**
 * A simple personnal tutorial for my first baby steps with Rust.
 */
// TODO refactor and unit tests to improve code while knowledge grows !
// TODO handle remote http feed discovery by parsing index and search for the feed link
// TODO serialize to local document database CRUD

// use log::{debug, info, warn, error };
// use log::{error, info};
mod args;
use crate::args::parse_args;

mod channel_lib;
use crate::channel_lib::{build_channel, Channel};

mod json_lib;
use crate::json_lib::{build_json, JsonValue};

mod render;
use crate::render::render_json;

fn main() {
    env_logger::init();

    let args = parse_args();

    let channel: Channel = build_channel(&args.input);

    let data: JsonValue = build_json(&channel);

    render_json(&data, args.pretty, args.indentation, &args.output);
}
