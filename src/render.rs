use json::JsonValue;
use log::{error, info};
use std::fs;

pub fn render_json(value: &JsonValue, pretty: bool, spaces: u16, output: &String) {
    // output result
    let output_string: String = if pretty {
        value.pretty(spaces)
    } else {
        value.dump()
    };

    if output.len() > 0 {
        let output_length = output_string.len();
        let filename = output.to_string();
        match fs::write(filename, output_string) {
            Ok(_) => {
                info!("saved {} characters to {}", output_length, output);
            }
            Err(e) => {
                error!("{:?}", e);
            }
        }
    } else {
        // no file name specified, dump json to stdout
        println!("{}", output_string);
    }
}
