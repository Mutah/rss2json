# rss2json

rss2json converts rss xml feeds to a json format.

## About

**This is my first Rust tutorial project**, so surely not coded with all the best practices, and clearly useless.

Output format is a complete personnal choice, inspired by some formats I've seen when using other's api. 

>It handles a *very* minimal subset and not pretends to fullfill industrial needs :)

## Installation

Setup Rust using [rustup](https://www.rust-lang.org/tools/install) and [set channels to nigthly](https://rust-lang.github.io/rustup/concepts/channels.html).

Then run in a terminal:

```bash
cargo install  --git https://git.tetalab.org/Mutah/rss2json.git
```



## Usage

```bash
Usage: rss2json.exe [OPTIONS] --input <INPUT>

Options:
  -i, --input <INPUT>
  -o, --output <OUTPUT>  [default: ]
  -p, --pretty
  -h, --help             Print help
  -V, --version          Print version
```

### Local file

```bash
rss2json -i some_downloaded_rss_file.xml
```

### Remote

```bash
rss2json -i https://git.tetalab.org/Mutah/rss2json.rss
```

## Output format

Only translate a minimal subset of attributes (if available).

```javascript
{
    "channel": {
        "title": "",
        "link": "",
        "description": "",
        "last_build_date": "",
        "language": "",
        "copyright": "",
        "generator": ""
    },
    "items": [
        {
        "title": "",
        "link": "",
        "pub_date": "Sun, 14 Jul 2024 19:53:25 +0200",
        "pub_ts": 1720979605,
        "description": "",        
        "categories": ["", ""]
        }
    ]
}
```